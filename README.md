# powerline-gen

Powerline-style prompt generator

## Usage

Example usage:

```ruby
require "powerline-gen"

powerline = PowerlineGen.make do
  pointing left if rand(0..1) == 0

  separator
  label(
    text: "Hello world!",
    text_color: :black,
    background_color: :cyan
  )
  separator opposite_direction: true
  label(
    text: "test",
    text_color: :white,
    background_color: :blue
  )
  separator
  label(
    text: Dir.current,
    text_color: :white,
    background_color: :green
  )
  arrow color: :dark_gray, force_direction: left
  label(
    text: `whoami`.chomp,
    text_color: :black,
    background_color: :white,
    spacing: ""
  )
  arrow color: :dark_gray, force_direction: right

  arrow color: :light_gray
  arrow color: :dark_gray
end

puts powerline

```

## Contributors

- [Zatherz](https://gitlab.com/u/zatherz) - creator, maintainer
