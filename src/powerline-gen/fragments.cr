class PowerlineGen
  abstract struct Fragment
    abstract def render(io : IO, direction : Direction, bg_color : Symbol, label_spacing : String, prev_frag : Fragment?, next_frag : Fragment?)
  end

  record Label < Fragment,
    text : String,
    text_color : Symbol,
    background_color : Symbol? = nil,
    spacing : String? = nil do
    def render(io, direction, bg_color, label_spacing, prev_frag, next_frag)
      label_spacing = spacing || label_spacing
      io << "#{label_spacing}#{text}#{label_spacing}".colorize.fore(text_color).back(background_color || bg_color)
    end
  end

  record Arrow < Fragment,
    color : Symbol,
    force_direction : Direction? = nil,
    opposite_direction = false do
    def render(io, direction, bg_color, label_spacing, prev_frag, next_frag)
      direction = force_direction || direction
      direction = direction.opposite if opposite_direction

      left_bg_color = bg_color
      if prev_frag.is_a?(Label)
        left_bg_color = prev_frag.background_color || bg_color
      end
      left_bg_color = prev_frag.color if prev_frag.is_a?(Arrow)

      right_bg_color = bg_color
      if next_frag.is_a?(Label)
        right_bg_color = next_frag.background_color || bg_color
      end
      right_bg_color = next_frag.color if next_frag.is_a?(Arrow)

      if prev_frag.is_a?(Arrow)
        if direction.right?
          io << Symbols::RIGHT_FILLED_ARROW.colorize.fore(color).back(right_bg_color)
        else
          io << Symbols::LEFT_FILLED_ARROW.colorize.fore(right_bg_color).back(color)
        end
      else
        if direction.right?
          io << Symbols::RIGHT_FILLED_ARROW.colorize.fore(left_bg_color).back(color)
          io << Symbols::RIGHT_FILLED_ARROW.colorize.fore(color).back(right_bg_color)
        else
          io << Symbols::LEFT_FILLED_ARROW.colorize.fore(color).back(left_bg_color)
          io << Symbols::LEFT_FILLED_ARROW.colorize.fore(right_bg_color).back(color)
        end
      end
    end
  end

  record Separator < Fragment,
    pointy = true,
    spacing = false,
    spacing_color : Symbol? = nil,
    force_direction : Direction? = nil,
    opposite_direction = false do
    def render(io, direction, bg_color, label_spacing, prev_frag, next_frag)
      direction = force_direction || direction
      direction = direction.opposite if opposite_direction

      return if !pointy
      if !spacing && prev_frag.is_a?(Label) && next_frag.is_a?(Label)
        if direction.right?
          io << Symbols::RIGHT_FILLED_ARROW.colorize.fore(prev_frag.background_color || bg_color).back(next_frag.background_color || bg_color)
        else
          io << Symbols::LEFT_FILLED_ARROW.colorize.back(prev_frag.background_color || bg_color).fore(next_frag.background_color || bg_color)
        end

        return
      end

      if prev_frag.is_a?(Label)
        if direction.right?
          io << Symbols::RIGHT_FILLED_ARROW.colorize.fore(prev_frag.background_color || bg_color).back(spacing_color || bg_color)
        else
          io << Symbols::LEFT_FILLED_ARROW.colorize.back(prev_frag.background_color || bg_color).fore(spacing_color || bg_color)
        end
      end

      if next_frag.is_a?(Label)
        if direction.right?
          io << Symbols::RIGHT_FILLED_ARROW.colorize.fore(spacing_color || bg_color).back(next_frag.background_color || bg_color)
        else
          io << Symbols::LEFT_FILLED_ARROW.colorize.back(spacing_color || bg_color).fore(next_frag.background_color || bg_color)
        end
      end
    end
  end
end
