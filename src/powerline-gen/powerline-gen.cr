class PowerlineGen
  enum Direction
    Left
    Right

    def left?
      self == Left
    end

    def right?
      self == Right
    end

    def opposite
      left? ? Right : Left
    end
  end

  @fragments = [] of Fragment
  @direction = Direction::Right
  @bg_color = :black
  @label_spacing = " "

  def self.make : self
    self.new.tap { |p| with p yield }
  end

  def separator(*args, **kwargs)
    Separator.new(*args, **kwargs).tap { |s| @fragments << s }
  end

  def label(*args, **kwargs)
    Label.new(*args, **kwargs).tap { |l| @fragments << l }
  end

  def arrow(*args, **kwargs)
    Arrow.new(*args, **kwargs).tap { |l| @fragments << l }
  end

  def fragment(frag : Fragment)
    @fragments << frag
  end

  def pointing(dir : Direction)
    @direction = dir
  end

  def left
    Direction::Left
  end

  def right
    Direction::Right
  end

  def bg_color_is(color : Symbol)
    @bg_color = color
  end

  def with_label_spacing(spacing : String)
    @label_spacing = spacing
  end

  def render(io : IO)
    @fragments.each_with_index do |frag, i|
      frag.render io, @direction, @bg_color, @label_spacing, i > 0 ? @fragments[i - 1] : nil, @fragments[i + 1]?
    end
  end

  def render
    String.build do |str|
      render str
    end
  end

  def to_s(io)
    render io
  end
end
