class PowerlineGen
  module Symbols
    VERSION_CONTROL_BRANCH = '\u{e0a0}'
    LN                     = '\u{e0a1}'
    PADLOCK                = '\u{e0a2}'

    RIGHT_FILLED_ARROW = '\u{e0b0}'
    RIGHT_ARROW        = '\u{e0b1}'

    LEFT_FILLED_ARROW = '\u{e0b2}'
    LEFT_ARROW        = '\u{e0b3}'
  end

  {% for c in Symbols.constants %}
    def {{c.downcase}}
      Symbols::{{c.id}}
    end
  {% end %}
end
